def days_conversion(days_n_unit_dic):
    # Convert the number of days given into minutes
    conversion_unit = days_n_unit_dic["unit"]
    num_of_days = days_n_unit_dic["days"]

    if days_n_unit_dic["unit"] == "minutes":
        print(f"{num_of_days} days are equal to {days * 24 * 60} {conversion_unit}!")
        return f"{num_of_days} days are equal to {days * 24 * 60} {conversion_unit}!"
    elif days_n_unit_dic["unit"] == "seconds":
        print(f"{num_of_days} days are equal to {days * 24 * 60 * 60} {conversion_unit}!")
        return f"{num_of_days} days are equal to {days * 24 * 60 * 60} {conversion_unit}!"
    else:
        print("unsupported unit...")
        return "unsupported unit..."
    
def validate_n_execute(days_n_unit_dic):
    # Validates your input and call days_conversion function if passes.
    try:
        days = int(days_n_unit_dic["days"])
        if days > 0:
            return days_conversion(days_n_unit_dic)
        elif days == 0:
            return "0 days are equal to 0 minutes dude!"
        else:
            return "You didn't type a valid value"
    except ValueError:
        return "Your input is not a digit dude!"

input = input("Please enter number of days to convert and convertion unit: ")

days_n_unit = input.split(":")

days_n_unit_dic = {
    "days": days_n_unit[0],
    "unit": days_n_unit[1]
    }

days = int(days_n_unit_dic["days"])

validate_n_execute(days_n_unit_dic)

print("end of the program!")