from datetime import datetime

user_input = input("enter your goal with a deadline separated by colon: ")
input_list = user_input.split(":")

goal = input_list[0]
deadline_date = datetime.strptime(input_list[1], "%d.%m.%Y")

# Calculate how many days from now till deadline

today_date = datetime.today()

time_till = (deadline_date - today_date)
print(f" Dear user! Time remaining for your goal \"{goal}\" is: {time_till.days * 24} hours")
