import helper

days = input("How many days do you want to calculate in minutes? ")
list_of_days = days.split(",")
print(list_of_days)
print(set(list_of_days))

while days != "exit":
    # If your input is a list, it splits it and run validate_n_execute in each element of it.
    for d in set(days.split(",")):
        if d == "exit":
            break
        helper.validate_n_execute(d)
        print(helper.validate_n_execute(d))
    if "exit" in days.split(","):
        break
    days = input("How many days do you want to calculate in minutes? ")

print("end of the program!")