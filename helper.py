def days_to_minutes(days):
    # Convert the number of days given into minutes
    return f"{days} days are equal to {days * 24 * 60} minutes!"
    
def validate_n_execute(days):
    # Validates your input and call days_to_minutes function if passes.
    try:
        days = int(days)
        if days > 0:
            return days_to_minutes(days)
        elif days == 0:
            return "0 days are equal to 0 minutes dude!"
        else:
            return "You didn't type a valid value"
    except ValueError:
        return "Your input is not a digit dude!"